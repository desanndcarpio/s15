/*Create a let varible with the name of your favorite food*/
let favFood = 'sinigang';
console.log(favFood);

/*Create a let variable with the sum of 150 and 9.
let sum = 150 + 9;
*/
let num = 150;
let num1 = 9;

let sum = num + num1;
console.log(sum);

/*Create a let variable with the product of 100 and 90.
let product = 100 * 90;
*/
let num2 = 100;
let num3 = 90;

let product = num2 * num3;
console.log(product);

/*Create a let variable with boolean value.
	- this variable asks if the user is active.
let isActive = true;
	*/
let isActive= true;
console.log(isActive);

/*Create a group of data with the names of your 5 restaurants.
let favoriteRestaurant = [''];
*/
let favResto = ['Jollibee', 'Mcdo', 'KFC', 'Yellow Cab', 'Panda Express'];
console.log(favResto);


/*
Create a variable which describes your favorite musical artist/singer or actor/actress with the following key-value pair:
	firstName: 	<value>
	lastName: 	<value>
	stageName:	<value>
	birthDay:	<value>
	age:		<value>
	bestAlbum:	<value>/bestTVShow:	<value>
	bestSong:	<value>/bestMovie: <value>
	isActive:	<value>

	if the data is unavailable or there is actually NO value, add NULL.

let favArtist = {
	
};

*/
let favArtist = {
	firstName: 'Taylor Alison' ,
	lastName: 'Swift' ,
	stageName: 'Taylor Swift' ,
	birthDay: 'December 13, 1989' ,
	age: 31 ,
	bestAlbum: 'T.S. 1989' ,
	bestSong: 'Blank Space' ,
	isActive: true
}
console.log(favArtist);


/*
Create function able to receive two numbers as arguments.
	- display the quotient of the two numbers in the console.
	- return the value of the division

	Create a variable called quotient
	- this variable should be able to receive the result of division function.

Log the quotient variables's value in the console with the following message:
	"The result of the division is <valueOfQuotient>"
		- use concatenation/template literal
Solution:

	function divideNum (num1, num2){
 	// console.log(num1/num2); for debugging
 	return num1/num2;
 };

 let quotient = divideNum(50, 10);
 console.log(`The result of the division is: ${quotient}`);

	*/
 function divideNum (num4, num5){
 	return (`The result of the division is ${num4 / num5}`);
 }

 let quotient = divideNum(100, 5);
 console.log(quotient);


/*
Mathematical Operators

*/

let num_1 = 5 ;
let num_2 = 10;
let num_3= 4;
let num_4= 40;


//reassigned another value
//num_1 = num_1 + num_4; 

//shorthand method
num_1 += num_4;
console.log(num_1);

num_2 += num_4;
console.log(num_2);

num_1 *= 2;
console.log (num_1);

let string1 = "Boston";
let string2 = " Celtics";

//string1 = string1 + string2;
string1 += string2;
console.log(string1);

//num_1 = num_1 - string1;
num_1 -= string1;
console.log(num_1); //Nan

let string3 = "Hello everyone";
/*let myArray = string3.split("", 3);
console.log(myArray);*/

string3.slice(0, 5);   // Returns "Hello"
console.log(string3.slice(0, 5) );

/*follows MDAS*/
let mdasResult = 1 + 2 - 3 * 4 / 5 ;

/*
	3*4 = 12
	12/5 = 2.4
	1+2 = 3
	3-2.4 = 0.6
*/

 console.log(mdasResult);

 //PEMDAS

 let pemdasResult = 1 + (2-3) * (4/5);

 /*
	4/5 = 0.8
	2-3 = -1
	-1 * 0.8 = -0.8
	1 + -0.8 = .19
 */

 console.log(pemdasResult);

 //increment and decrement

 let z = 1;

 	//pre-fix
    ++z;
    console.log(z); //2

    //post-fix
    z++;
    console.log(z); //3

    console.log(z++); //still 3 because value first before incrementation

    console.log(z); 

let n = 1;

//pre-fix
console.log(++n); //adding 1 before the value 1 + n = 2


//post-fix
console.log(n++); //return value first n + 1 = 3 but still 2 in console
console.log(n); //value 3 

//pre-fix and post-fix decrementation

//prefix
console.log(--z); //3

//postfix 
console.log(z--); //3
console.log(z); //2


/*
	Comparison Operators

*/

/*- Equality or Loose Equality Operator ( == )*/
        
    console.log (1 == 1); //true
    console.log('1' == 1); //true , loose equality operator - comparing only the value
    console.log('1' === 1); //false, strict equality operator - comparing also the data type

    console.log('apple' == 'Apple'); //false case sensitive


let isSame = 55 == 55;//true
console.log(isSame);

console.log(0 == false); //true true = 1 false = 0 forced coercion
console.log(1 == true); //true true = 1 false = 0 forced coercion
console.log(true == 'true'); //false true = 1 != 'true' = NaN forced coercion

console.log(true == '1'); //true true = 1 '1'=1 forced coercion
console.log('0' == false); //true forced coercion


//strict equality
console.log(1 === '1'); //false different data type
console.log('Juan' === 'Juan'); //true same value same type
console.log('Maria' === 'maria'); //false not same value


/*Inequality*/

//loose inequality (!=)

console.log('1' != 1); //false - both operands are converted to numbers not inequal
console.log('James' != 'John'); //true 
console.log(1 != true); //false they are equal forced coercion
console.log(1 != "true"); //true not equal 1 != NaN

//strict inequality (!==)

console.log('5' !== 5); //true different types
console.log(5 !== 5); //false same value same type


let name1 = 'Juan';
let name2 = 'Maria';
let name3 = 'Pedro';
let name4 = 'Perla';

let number1 = 50;
let number2 = 60;
let numString1 = "50";
let numString2 = "60";

console.log(numString1 == number1); //true
console.log(numString1 === number1); //false
console.log(numString1 != number1); //false
console.log(name4 !== name3); //true
console.log(name1 == 'juan'); //false
console.log(name1 === "Juan"); //true


/*
	Relational Comparison Operators
*/


let x = 500;
let y = 700; 
let w = 8000;
let numString3 = "5500";

//Greater Than ( > )

console.log( x > y); //false 500 > 700
console.log( w > y); //true 8000 > 700

//Less Than ( < )
console.log(w < y); //false 8000 < 700
console.log(y < y); //false 700 < 699  699 n-1
console.log(y > y); //false 699 > 700 699 n-1
console.log(y >= y); //true 700 >= 700
console.log(x < 1000); //true
console.log(numString3 < 1000); //false forced coercion
console.log(numString3 < 6000); //true 
console.log(numString3 < 'Juan'); // true erratic/logical error unpredictable, uneven
console.log(numString3 > 'Juan'); // false logic error
console.log(6000 < 'Juan') //false

/*
	Logical Operators
*/

let isAdmin = false;
let isRegistered = true;
let isLegalAge = true;
let isVoter = false;

let authorization1 = isAdmin && isRegistered;
console.log(authorization1); // false F && T

let authorization2 = isLegalAge && isRegistered;
console.log(authorization2); //true T && T

let authorization3 = isAdmin && isLegalAge;
console.log(authorization3); //false F && T

let authorization = isAdmin && isVoter;
console.log(authorization); //false F && F

let requiredLevel = 95;
let requiredAge = 18;

let authorization4 = isRegistered && requiredLevel === 25;
console.log(authorization4); //false required level === 25 (false)

let authorization5 = isRegistered && isLegalAge && requiredLevel === 95; 
console.log(authorization5); //true 

let userName1 = 'gamer2021';
let userName2 = 'shadowMaster';
let userAge1 = 15;
let userAge2 = 30;

let registration1 = userName1.length > 9 && userAge1 >= requiredAge;
// .length - property of string which determines the number of characters in the string
console.log(registration1); //false  9 > 9 F && 15 >= 18 F

let registration2 = userName2.length > 8 && userAge2 >= requiredAge;
console.log(registration2); //true 12 > 8 T && 30 >= 18 T

let registration3 = userName1.length > 8 && userAge2 >= requiredAge;
console.log(registration3); //true 9 > 8 T && 30 > 18 T


/*
	OR Operator ( || )

*/

let userLevel = 100;
let userLevel2 = 65;

let guildRequirement = isRegistered && userLevel >= requiredLevel && userAge1 >= requiredAge; // false true && T && F
console.log(guildRequirement); 

guildRequirement = isRegistered || userLevel >= requiredLevel || userAge1 >= requiredAge; // true true || T || F
console.log(guildRequirement); 

let guildRequirement2 = userLevel >= requiredLevel || userAge1 >= requiredAge;
console.log(guildRequirement2);

let guildAdmin = isAdmin || userLevel2 >= requiredLevel;
console.log(guildAdmin); // false false || F 


/*
	Not Operator (!)
*/

let guildAdmin1 = !isAdmin || userLevel2 >= requiredLevel;
console.log(guildAdmin1); //true

let opposite1 = !isAdmin; //!false-> true
let opposite2 = !isLegalAge; //!true -> false 

console.log(opposite1);
console.log(opposite2); 


/*
	if-else

*/

const candy = 100;

if(candy >=100){
	console.log('You got a cavity!')
};


/*

	if(true){
			block of code
	};

*/

let userName3 = 'crusader_1993';
let userLevel3 = 25;
let userAge3 = 30;

if (userName3.length > 10){
	console.log('Welcome to the Game online!')
};


/*
	else 

*/
 
if(userName3 >=10 && userLevel3 <= 25 && userAge3 >= requiredAge){
		console.log('Thank you for joining Noobies Guild!')
} else {
	console.log('You too strong to be a noob. :(')
};


/*
	else if

*/

if(userName3.length >= 10 && userLevel3 <= 25 && userAge3 >= requiredAge){
	console.log("Thank you for joining the noobies guild.")
} else if (userLevel3 > 25){
	console.log("You too string to be noob. :(")
} else if(userAge3 < requiredAge){
	console.log("You're too young to join the guild.")
} else {
	console.log("End of the condition.")
}

/*
	if-else in function

*/

function addNum(num1, num2){
	if (typeof num1 === 'number' && typeof num2 === 'number'){
		console.log("Run only if both arguments passed are number types.")
		console.log(num1 + num2);
	} else {
		console.log("One or both of the arguments are not numbers.")
	};
};

addNum(5,2);

/*


let customerName = prompt("Enter youe name: ");
if(customerName != null){
	document.getElementById('username').value = customerName;
}*/


		/*
			Nested if-else if
			Mini-Activity
				add another condition to our nested if statement:
					- check if the password is at least 8 characters long
				add an else statement which will run if both conditions we not met:
					- show an alert which says "Credentials too short."

			Stretch Goals:

			add an else if statement that if the username is less than 8 characters
				- show an alert message "username is too short."
			add an else if statement that if the password is less than 8 characters
				- show an alert messge "password too short"
			
			Push it in Gitlab and paste the URL in our Boodle account s15

		 */

function login(username, password){
	if(typeof username === 'string' && typeof password === 'number'){
		console.log('both arguments are string.')
	}  else if (password.length < 8 && username.length < 8) {
		console.log('Credentials too short');
		alert('Credentials too short');
	} else if (username.length < 8){
		console.log('username is too short');
		alert('username is too short');
	} else if (password.length < 8){
		console.log('password too short');
		alert('password too short');
	} else {
		console.log('reenter credentials')
	}
};

login('aasa', 'asef' );